/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo5_p1_campana_pisco_santilla;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 *
 * @author Dustin Pisco
 * @author Karen Santillan
 */
public class Paciente extends Usuario{
    private String cedula;
    private String f_nacimiento;
    private double peso;
    private int edad;
    private double estatura;
    private char genero;
    
    public Paciente(String nombre, String apellidos, String usuario, String contraseña, char tipo) {
        super(nombre, apellidos, usuario, contraseña, tipo);
    }
    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
    public String getFnacimiento() {
        return f_nacimiento;
    }

    public void setFacimiento(String fecha) {
        this.f_nacimiento = fecha;
    }
    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }
    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
    public double getEstatura() {
        return estatura;
    }

    public void setEstatura(double estatura) {
        this.estatura = estatura;
    }
    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }
    /**
     * metodo que nos sirve para que el paciente agende una cita en el sistema
     * void()
     **/
    public void solicitarCita(){
        Cita cita = null ;
        Paciente paciente=null;
        String especialidad=null;
       
        int iterador = 1;
        int iterador1 = 1;
        ArrayList<Doctor> general = new ArrayList();
        ArrayList<String> opciones = new ArrayList();
        ArrayList<String> horarios = new ArrayList();
        ArrayList<String> fechas = new ArrayList();
        ArrayList<String> especialidades = new ArrayList();
        general.add(new Doctor("Juan Mendoza", "Cardiologo", "17/06/2020","09:00"));
        general.add(new Doctor("Carlos Alcivar", "Nutriologo", "18/06/2020","15:00"));
        general.add(new Doctor("Juan Mendoza", "Gastroenterología", "19/06/2020","11:00"));
        Scanner sc = new Scanner(System.in);
        int conteo=1;
        System.out.println("especialidades");
        for (Doctor doctor:general) {
            System.out.println(conteo+doctor.getespecialidad());
            especialidades.add(doctor.getespecialidad());
            conteo++;
        }
        System.out.println("elija una opcion:");
        int opcion=sc.nextInt();
        

        System.out.print("Los doctores especializados en el campo son:\n");
        String doc=null;
        for (Doctor doctor: general){
            if (doctor.getespecialidad().equals(especialidades.get(opcion-1))){
                opciones.add(doctor.getnombre());
                System.out.println(iterador + " " +doctor.getnombre());
                iterador +=1;
            }}
        System.out.print("Seleccione la opcion de doctor: ");  
        int opciond = sc.nextInt();
        String name = opciones.get(opciond-1);
//        cita.setDoctor(name);
        doc=name;
        System.out.print("/********************HORARIOS DISPONIBLES PARA CITAS********************/ \n"
                       + "/*                                                                     */ \n"
                       + "/***********************************************************************/\n");
        for (Doctor doctor: general){
            if (doctor.getnombre().equals(name)){
                System.out.println( iterador1+". "+doctor.getfecha() +"." +doctor.gethorario());
                iterador1 +=1;
                horarios.add(doctor.gethorario());
                fechas.add(doctor.getfecha());
            }
        }
        System.out.print("Elija una opcion: ");
        int opcion1 = sc.nextInt();
        String fecha = fechas.get(opcion1-1);
//        cita.setFecha(fecha);
        String horario = horarios.get(opcion1-1);
//        cita.setHorario(horario);
        
        //Para generar el numero aleatorio utilizaremos la libreria random
        int numero = (int)(Math.random()*1000);
        System.out.println("Su cita se ha reservado con el codigo: "+numero);
//        cita.setCodigo(String.valueOf(numero));
        System.out.print("Desea solicitar la cita con los datos seleccionados?: ");
        String si=sc.next();
        sc.nextLine();
        if (si.toUpperCase().equals("S")){
//            String contenido = "codigo,especialidad,doctor,paciente,fecha,horario \n";
//            String contenido =cita.getCodigo()+"," +especialidades.get(opcion-1)+","+doc+","+ paciente.getnombre()+","+fecha+","+horario;    
            Usuario.EscribirArchivo("citas_medicas.txt", numero+"," +especialidades.get(opcion-1)+","+doc+","+ paciente.getnombre()+","
                    +fecha+","+horario);
        }
            
    }
    /**
     * el metodo nos sirve para que el paciente pueda consultar una cita pendiente 
     **/
    public void consultarCita(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese codigo de cita: ");
        String Codigo = sc.nextLine();
//        sc.nextLine();
        System.out.print("/*********************CITA PENDIENTE***************/\n");
        System.out.print("/*                                                */\n");
        System.out.print("/**************************************************/\n");
        ArrayList<String> citConsu=LeeFichero("citas_medicas.txt");
        String codigo=null;
        String especialidad = null;
        String doctor = null;
        String fecha = null;
        String horario = null;
        String nombre=null;
        for (String consult:citConsu){
            String[] listaconsult = consult.split(",");
            codigo=listaconsult[0];
            especialidad=listaconsult[1];
            doctor=listaconsult[2];
            fecha=listaconsult[4];
            horario=listaconsult[5];
            nombre=listaconsult[3];
        }
            if (codigo.equals(Codigo)){
                System.out.print("ESPECIALIDAD: " + especialidad+"\n");
                System.out.print("DOCTOR: " + doctor+"\n");
                System.out.print("FECHA: " + fecha+"\n");
                System.out.print("HORARIO: " + horario+"\n");
            
                ArrayList<String> usua=LeeFichero("Usuarios.txt");
                for (String usuarios: usua){
                    String[] listausua= usuarios.split(";");
                   String usuario = listausua[0];
                    if(nombre.equals(usuario)){
                      String tipo = listausua[4]; 
                      if (tipo.equals("E")){
                         System.out.print("Como usted es un paciente estándar,"
                                 + " se han debitado 25.00 dolares de su cuenta\n");
                      }
                      else{
                           System.out.print("Como usted es un paciente preferencial,"
                                 + " se han debitado 25.00 dolares de su cuenta\n");
                      }
                    } 
                }         
            }
            else{
                System.out.print("Codigo incorrecto, porfavor ingrese otro codigo\n");
                System.out.print("Ingrese codigo de cita: ");
                Codigo = sc.nextLine();
            }
            
          
        }
            
        
    /**
     * el metodo nos permite que un paciente solicite la cancelacion de la cita
     **/
    public void solicitarCancelacion(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese codigo de cita a cancelar: ");
        String Codigo = sc.nextLine();
        ArrayList<String> canConsu = LeeFichero("citas_medicas.txt");
        String cita=null;
        String nombre = null;
        for (String cance: canConsu){
            String[] listacance = cance.split(",");
            cita=listacance[0];
            nombre=listacance[3];
        }
            if (cita.equals(Codigo)){
                System.out.print("La solicitud ha sido enviada, cuando el asistente lo revise, cancelará su cita\n");
                String dato = cita +","+nombre;
                EscribirArchivo("solicitudCancelacion.txt", dato);
            }
            else{
                System.out.print("Codigo incorrecto, porfavor ingrese otro codigo: ");
            }
        
    }  
}
