/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo5_p1_campana_pisco_santilla;

import java.util.ArrayList;
import java.util.Scanner;
import static poo5_p1_campana_pisco_santilla.Hospital.EscribirArchivo;

/**
 *
 * @author Dustin Pisco
 */
public class PacienteEstandar extends Paciente {
    private String numero_cuenta;
    public PacienteEstandar(String nombre, String apellidos, String usuario, String contraseña, char tipo) {
        super(nombre, apellidos, usuario, contraseña, tipo);
    }
    public String getCuenta() {
        return numero_cuenta;
    }

    public void setCuenta(String cuenta) {
        this.numero_cuenta = cuenta;
    }
    /**
     * metodo que nos sirve para que el paciente Estandar agende una cita en el sistema
     * @param paciente
     **/
    public void solicitarCita(Paciente paciente){
        boolean citaEstandar = true;
        ArrayList<String> lista_citas = LeeFichero("citas_medicas.txt");
        for (String citas:lista_citas) {
            String[] lista=citas.split(",");
            if (lista[3].equals(paciente.getnombre())) {
                System.out.println("usted ya tiene una cita pendiente, por lo que no puede solicitar una nueva");  
                citaEstandar=false;
            }
                
            
        }
        while (citaEstandar==true){
//        Paciente paciente=null;
        Cita cita = null ;
        String especialidad=null;
       
        int iterador = 1;
        int iterador1 = 1;
        ArrayList<Doctor> general = new ArrayList();
        ArrayList<String> opciones = new ArrayList();
        ArrayList<String> horarios = new ArrayList();
        ArrayList<String> fechas = new ArrayList();
        ArrayList<String> especialidades = new ArrayList();
        general.add(new Doctor("Juan Mendoza", "Cardiologo", "17/06/2020","09:00"));
        general.add(new Doctor("Carlos Alcivar", "Nutriologo", "18/06/2020","15:00"));
        general.add(new Doctor("Juan Mendoza", "Gastroenterología", "19/06/2020","11:00"));
        Scanner sc = new Scanner(System.in);
        int conteo=1;
        System.out.println("especialidades");
        for (Doctor doctor:general) {
            System.out.println(conteo+doctor.getespecialidad());
            especialidades.add(doctor.getespecialidad());
            conteo++;
        }
        System.out.println("elija una opcion:");
        int opcion=sc.nextInt();
        

        System.out.print("Los doctores especializados en el campo son:\n");
        String doc=null;
        for (Doctor doctor: general){
            if (doctor.getespecialidad().equals(especialidades.get(opcion-1))){
                opciones.add(doctor.getnombre());
                System.out.println(iterador + " " +doctor.getnombre());
                iterador +=1;
            }}
        System.out.print("Seleccione la opcion de doctor: ");  
        int opciond = sc.nextInt();
        String name = opciones.get(opciond-1);
//        cita.setDoctor(name);
        doc=name;
        System.out.print("/********************HORARIOS DISPONIBLES PARA CITAS********************/ \n"
                       + "/*                                                                     */ \n"
                       + "/***********************************************************************/\n");
        for (Doctor doctor: general){
            if (doctor.getnombre().equals(name)){
                System.out.println( iterador1+". "+doctor.getfecha() +"." +doctor.gethorario());
                iterador1 +=1;
                horarios.add(doctor.gethorario());
                fechas.add(doctor.getfecha());
            }
        }
        System.out.print("Elija una opcion: ");
        int opcion1 = sc.nextInt();
        String fecha = fechas.get(opcion1-1);
//        cita.setFecha(fecha);
        String horario = horarios.get(opcion1-1);
//        cita.setHorario(horario);
        
        //Para generar el numero aleatorio utilizaremos la libreria random
        int numero = (int)(Math.random()*1000);
        System.out.println("Su cita se ha reservado con el codigo: "+numero);
//        cita.setCodigo(String.valueOf(numero));
        System.out.print("Desea solicitar la cita con los datos seleccionados?: ");
        String si=sc.next();
        sc.nextLine();
        if (si.toUpperCase().equals("S")){
//            String contenido = "codigo,especialidad,doctor,paciente,fecha,horario \n";
//            String contenido =cita.getCodigo()+"," +especialidades.get(opcion-1)+","+doc+","+ paciente.getnombre()+","+fecha+","+horario;    
            Usuario.EscribirArchivo("citas_medicas.txt", numero+"," +especialidades.get(opcion-1)+","+doc+","+ paciente.getnombre()+","
                    +fecha+","+horario);
        }
            
    }
    }
        
}
