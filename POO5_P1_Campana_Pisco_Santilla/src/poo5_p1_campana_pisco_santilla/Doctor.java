/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo5_p1_campana_pisco_santilla;

/**
 *
 * @author Cliente
 */
public class Doctor {
    private String nombreDoc;
    private String especialidad;
    private String fecha;
    private String horario;
    
    
    public Doctor (String nombreDoc, String especialidad, String fecha, String horario){
        this.nombreDoc = nombreDoc;
        this.especialidad = especialidad;
        this.fecha = fecha;
        this.horario = horario;
    }
    public String getnombre(){
        return nombreDoc;
    }
    public void setnombre(String nombre){
        this.nombreDoc = nombreDoc;
    }
    public String getespecialidad(){
        return especialidad;
    }
    public void setespecialidad(String especialidad){
        this.especialidad = especialidad;
    }
    public String getfecha(){
        return fecha;
    }
    public void setfecha(String fecha){
        this.fecha = fecha;
    }
    public String gethorario(){
        return horario;
    }
    public void sethorario(String horario){
        this.horario = horario;
    }
}

