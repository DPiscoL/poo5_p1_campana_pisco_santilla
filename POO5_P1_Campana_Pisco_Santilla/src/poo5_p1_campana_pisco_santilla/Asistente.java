/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo5_p1_campana_pisco_santilla;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Karen Santillan
 */
public class Asistente extends Usuario{

    public Asistente(String nombre, String apellidos, String usuario, String contraseña, char tipo) {
        super(nombre, apellidos, usuario, contraseña, tipo);
        
    }
    /**
     * Este metodo sirve para que un asistente llene los datos de un paciente
     * @param usuarios ArrayList requerrido para encontrar al paciente
     **/
    public void llenarDatos(ArrayList<String> usuarios){
        System.out.println("++++++++++++++++++++++++++++++\n");
         System.out.println("      LLENAR PERFIL PACIENTE");
         System.out.println("++++++++++++++++++++++++++++++");
          Scanner sc= new Scanner(System.in);
         System.out.print("Ingrese el usuario:");
         String usu=sc.nextLine();
         while(validarUsuario(usu)==false){
             System.out.println("usuario invalido.....");
             System.out.print("Ingrese el usuario:");
             usu=sc.nextLine();
        }
         Paciente paciente = null;
         for (String usuario:usuarios) {
         String[] listaUsuario = usuario.split(";");
             if (listaUsuario[2].equals(usu)) {
                  paciente=new Paciente(listaUsuario[0], listaUsuario[1], listaUsuario[2], listaUsuario[3],listaUsuario[4].charAt(0));
             }
       
         }
        System.out.println("Actualice los datos para "+usu); 
        System.out.print("Ingrese el cedula:");
        String cedula=sc.nextLine();
        paciente.setCedula(cedula);
        System.out.print("Ingrese edad:");
        int edad=sc.nextInt();
        paciente.setEdad(edad);
        System.out.print("Ingrese el peso (kg):");
        double peso=sc.nextDouble();
        paciente.setPeso(peso);
        System.out.print("Ingrese la estatura(mt):");
        double estatura=sc.nextDouble();
        paciente.setEstatura(estatura);
        System.out.print("Ingrese fecha de nacimiento (dd/mm/aa):");
        String fecha=sc.nextLine();
        sc.nextLine();
        paciente.setFacimiento(fecha);
        System.out.print("Ingrese el genero (M/F):");
        String genero=sc.nextLine();
        paciente.setGenero(genero.charAt(0));
        EscribirArchivo("perfilPaciente.txt",usu+","+cedula+","+edad+","+peso+","+estatura+","+fecha+","+genero);
           
    }
    public void cancelar(){
    }
    /**
     * metodo que nos muestra por pantalla una lista de pacientes con citas pendientes
     **/
    public void consultarCitaPacientes(){
        System.out.println("\n*********PACIENTES CON CITAS MEDICAS*********\n"+"\n"
                + "*******************************************");
        ArrayList<String> usuarios = LeeFichero("Usuarios.txt");
        ArrayList<String> lista_citas = LeeFichero("citas_medicas.txt");
        ArrayList<String> perfiles = LeeFichero("perfilPaciente.txt");
        ArrayList<String> listaUsuarios = new ArrayList<String>();
        ArrayList<String> pacientes_cita = new ArrayList<String>();
        ArrayList<Integer> edades = new ArrayList<Integer>();
        
      
        //String usu=null;
        //String nombrePaciente = null;
        for (String cita : lista_citas) {
            String[] listacita = cita.split(",");
            pacientes_cita.add(listacita[3]);
            
       } 
        
        for (int i = 0; i <pacientes_cita.size() ; i++) {
            for (int j = 0; j < usuarios.size(); j++) {
            String[] listaUsuario = usuarios.get(j).split(";");
            String nombre=pacientes_cita.get(i);
            if (listaUsuario[0].equals(nombre)) {
                listaUsuarios.add(listaUsuario[2]);
           }
           }
        }
        for (int i = 0; i <perfiles.size() ; i++) {
            for (int j = 0; j < usuarios.size(); j++) {
            String[] listaperfiles =perfiles.get(i).split(",");
            String usuario=listaUsuarios.get(j);
            if (listaperfiles[0].equals(usuario)) {
                edades.add(Integer.parseInt(listaperfiles[2]));
            }
            }
        }
        for (int i = 0; i < pacientes_cita.size(); i++) {
            System.out.println(pacientes_cita.get(i)+","+edades.get(i)+" años");
            
            
        }
        
}
}