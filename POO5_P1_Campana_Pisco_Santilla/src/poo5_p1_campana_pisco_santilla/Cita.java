/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo5_p1_campana_pisco_santilla;

/**
 *
 * @author Dustin Pisco
 */
public class Cita {
    private String codigo;
    private String especialidad;
    private String doctor;
    private String paciente;
    private String fecha;
    private String horario;
    public Cita(String codigo,String especialidad,String doctor,String paciente,String fecha,String horario) {
        this.codigo=codigo;
        this.doctor=doctor;
        this.especialidad=especialidad;
        this.fecha=fecha;
        this.horario=horario;
        this.paciente=paciente;         
    }
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }
    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doc) {
        this.doctor = doc;
    }
    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }
    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha= fecha;
    }
    public String getHorario() {
        return horario;
    }

    public void setHorario(String hora) {
        this.horario = hora;
    } 
}
