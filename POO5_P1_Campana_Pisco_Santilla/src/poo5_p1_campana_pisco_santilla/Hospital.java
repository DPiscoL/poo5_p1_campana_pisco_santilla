/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo5_p1_campana_pisco_santilla;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Dustin Pisco
 * @author Andres Campaña
 * @author Karen Santillan
 */
public class Hospital {
    /**
     * Este metodo devuelve un metodo que crea un archivo y escribe una linea de texto
     * si el archivo esta creado solo escribe la linea
     * @param nombreArchivo nombre con el cual sera creado
     * @param linea lo que se quiere escribir
     **/
        public static void EscribirArchivo(String nombreArchivo, String linea) {

        FileWriter fichero = null;
        BufferedWriter bw = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter(nombreArchivo,true);
            bw = new BufferedWriter(fichero);
            bw.write(linea+"\n");
            System.out.println("se ha escrito con exito");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero) {
                    //fichero.close();
                    bw.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
        /**
     * Este metodo lee un archivo de texto para retornar un ArrayLlist
     * @param nombrearchivo String requerrido para que el programa reconosca el archivo
     * @return ArrayList
     **/
    public static ArrayList<String> LeeFichero(String nombrearchivo) {
        ArrayList<String> lineas = new ArrayList<>();
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;

        try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            archivo = new File(nombrearchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            while ((linea = br.readLine()) != null) {
                //System.out.println(linea);
                lineas.add(linea);

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si todo va bien como si salta 
            // una excepcion.
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return lineas;

    }
    /**
     * Este metodo devuelve un boolean y sirve para verificar si el usuario y contrase;a ingresado existe 
     * @param usuariop parametro a validar
     * @param contraseña parametros necesarios para validar 
     * @return boolean
     **/
    public static boolean validarUsuario(String usuariop,String contraseña){
        ArrayList<String> usuarios=LeeFichero("Usuarios.txt");
        for (String usuario:usuarios) {
            String[] listaUsuario = usuario.split(";");
            if (listaUsuario[2].equals(usuariop)&& listaUsuario[3].equals(contraseña)) {
                return true;
            }
        }
        return false; 
    }
    /**
     * Este metodo devuelve un boolean y sirve para verificar si el usuario ingresado existe 
     * @param usuariop parametro necesario para validar 
     * @return boolean
     **/
        public static boolean validarUsuario(String usuariop){
        ArrayList<String> usuarios=LeeFichero("Usuarios.txt");
        for (String usuario:usuarios) {
            String[] listaUsuario = usuario.split(";");
            if (listaUsuario[2].equals(usuariop)){
                return true;
            }
        }
        return false; 
    }/**
     * Este metodo devuelve un String y sirve para verificar el tipo de paciente que es
     * @param usuariop usuario ha validar
     * @param contraseña contrase;a a validar
     * @return String
     **/
    public static String defTipo(String usuariop,String contraseña){
        String tipo=null;
        ArrayList<String> usuarios=LeeFichero("Usuarios.txt");
        for (String usuario:usuarios) {
            String[] listaUsuario = usuario.split(";");
            if (listaUsuario[2].equals(usuariop)&& listaUsuario[3].equals(contraseña)) {
                tipo=listaUsuario[4];
            }
        }
            return tipo;
           
      
    }
    /**
     * Este metodo devuelve un String y sirve para verificar el tipo de paciente que es
     * @param usuariop parametro necesario para validar 
     * @return String 
     **/
        public static String defTipo(String usuariop){
        String tipo=null;
        ArrayList<String> usuarios=LeeFichero("Usuarios.txt");
        for (String usuario:usuarios) {
            String[] listaUsuario = usuario.split(";");
            if (listaUsuario[2].equals(usuariop)) {
                tipo=listaUsuario[4];
            }
        }
            return tipo;
           
      
    }
     public static void main(String[] arg) {
        //EscribirArchivo("Usuarios.txt","Andres;campaña;hola;hola123;E");
//         
//EscribirArchivo("citas_medicas.txt","codigo,especialidad,doctor,paciente,fecha,horario");
         System.out.println("++++++++++++++++++++++++++++++\n");
         System.out.println("      BIENVENIDO AL SISTEMA");
         System.out.println("++++++++++++++++++++++++++++++");
         Scanner sc= new Scanner(System.in);
         System.out.print("usuario:");
         String usu=sc.nextLine();
         System.out.print("contraseña:");
         String con=sc.nextLine();
         while(validarUsuario(usu,con)==false){
             System.out.println("usuario o contraseña invalidos.....");
             System.out.print("usuario:");
             usu=sc.nextLine();
             System.out.print("contraseña:");
             con=sc.nextLine();  
         }
         if ( validarUsuario(usu,con)==true && defTipo(usu,con).equals("E")) {//utilizamos la funcion validar usuario y definir tipo para mostrar el menu correspondiente en este caso elmenu de paciente
             ArrayList<String> usuarios=LeeFichero("Usuarios.txt");
             PacienteEstandar paciente = null;
            for (String usuario:usuarios) {
            String[] listaUsuario = usuario.split(";");
            if (listaUsuario[2].equals(usu)&& listaUsuario[3].equals(con)) {
                paciente=new PacienteEstandar(listaUsuario[0], listaUsuario[1], listaUsuario[2], listaUsuario[3],listaUsuario[4].charAt(0));
                
            }
            }
             System.out.println("Usted es un paciente Estandar");
             System.out.println("\n*********MENU*********\n"
                              + "*******PACIENTE*******");
             System.out.println("1. Solicitar una cita medica\n"
                     + "2. Consultar cita pendiente\n"
                     + "3. Solicitar cancelacion de cita");
             System.out.print("Elija una opcion: ");
             int opcion=sc.nextInt();
             while (opcion > 3 || opcion == 0) {
                 System.out.println("opcion no valida......");
                 System.out.println("\n*********MENU*********\n"
                         + "*******PACIENTE*******");
                 System.out.println("1. Solicitar una cita medica\n"
                         + "2. Consultar cita pendiente\n"
                         + "3. Solicitar cancelacion de cita");
                 System.out.print("Elija una opcion: ");
                 opcion = sc.nextInt();
             }
             if (opcion == 1) {
                 paciente.solicitarCita(paciente);
             }
             if (opcion == 2) {
                 paciente.consultarCita();

             }
             if (opcion == 3) {
                 paciente.solicitarCancelacion();

             }
             System.out.println("Desea regresa al menu principal (S/N): ");
             String desicion=sc.next().toLowerCase();
             while (desicion.equals("s")) {
                 System.out.println("Usted es un paciente Estandar");
                  System.out.println("\n*********MENU*********\n"
                              + "*******PACIENTE*******");
             System.out.println("1. Solicitar una cita medica\n"
                     + "2. Consultar cita pendiente\n"
                     + "3. Solicitar cancelacion de cita");
             System.out.print("Elija una opcion: ");
             opcion=sc.nextInt();
             while (opcion > 3 || opcion == 0) {
                 System.out.println("opcion no valida......");
                 System.out.println("\n*********MENU*********\n"
                         + "*******PACIENTE*******");
                 System.out.println("1. Solicitar una cita medica\n"
                         + "2. Consultar cita pendiente\n"
                         + "3. Solicitar cancelacion de cita");
                 System.out.print("Elija una opcion: ");
                 opcion = sc.nextInt();
             }
             if (opcion == 1) {
                 paciente.solicitarCita(paciente);

             }
             if (opcion == 2) {
                 paciente.consultarCita();

             }
             if (opcion == 3) {
                 paciente.solicitarCancelacion();

             }
             System.out.println("Desea regresa al menu principal (S/N): ");
             desicion=sc.next().toLowerCase();
             } 
             
         } 
         if ( validarUsuario(usu,con)==true && defTipo(usu,con).equals("P")) {//utilizamos la funcion validar usuario y definir tipo para mostrar el menu correspondiente en este caso elmenu de paciente
             ArrayList<String> usuarios=LeeFichero("Usuarios.txt");
             PacientePreferencial paciente = null;
            for (String usuario:usuarios) {
            String[] listaUsuario = usuario.split(";");
            if (listaUsuario[2].equals(usu)&& listaUsuario[3].equals(con)) {
                paciente=new PacientePreferencial(listaUsuario[0], listaUsuario[1], listaUsuario[2], listaUsuario[3],listaUsuario[4].charAt(0));
                
            }
            }
             System.out.println("Usted es un paciente Preferencial");
             System.out.println("\n*********MENU*********\n"
                              + "*******PACIENTE*******");
             System.out.println("1. Solicitar una cita medica\n"
                     + "2. Consultar cita pendiente\n"
                     + "3. Solicitar cancelacion de cita");
             System.out.print("Elija una opcion: ");
             int opcion=sc.nextInt();
             while (opcion > 3 || opcion == 0) {
                 System.out.println("opcion no valida......");
                 System.out.println("\n*********MENU*********\n"
                         + "*******PACIENTE*******");
                 System.out.println("1. Solicitar una cita medica\n"
                         + "2. Consultar cita pendiente\n"
                         + "3. Solicitar cancelacion de cita");
                 System.out.print("Elija una opcion: ");
                 opcion = sc.nextInt();
             }
             if (opcion == 1) {
                 paciente.solicitarCita(paciente);
             }
             if (opcion == 2) {
                 paciente.consultarCita();

             }
             if (opcion == 3) {
                 paciente.solicitarCancelacion();

             }
             System.out.println("Desea regresa al menu principal (S/N): ");
             String desicion=sc.next().toLowerCase();
             while (desicion.equals("s")) {
                 System.out.println("Usted es un paciente Preferencial");
                  System.out.println("\n*********MENU*********\n"
                              + "*******PACIENTE*******");
             System.out.println("1. Solicitar una cita medica\n"
                     + "2. Consultar cita pendiente\n"
                     + "3. Solicitar cancelacion de cita");
             System.out.print("Elija una opcion: ");
             opcion=sc.nextInt();
             while (opcion > 3 || opcion == 0) {
                 System.out.println("opcion no valida......");
                 System.out.println("\n*********MENU*********\n"
                         + "*******PACIENTE*******");
                 System.out.println("1. Solicitar una cita medica\n"
                         + "2. Consultar cita pendiente\n"
                         + "3. Solicitar cancelacion de cita");
                 System.out.print("Elija una opcion: ");
                 opcion = sc.nextInt();
             }
             if (opcion == 1) {
                 paciente.solicitarCita(paciente);

             }
             if (opcion == 2) {
                 paciente.consultarCita();

             }
             if (opcion == 3) {
                 paciente.solicitarCancelacion();

             }
             System.out.println("Desea regresa al menu principal (S/N): ");
             desicion=sc.next().toLowerCase();
             } 
             
         }
         if (validarUsuario(usu,con)==true && defTipo(usu,con).equals("A")){//implementacion menu asistente
              ArrayList<String> usuarios=LeeFichero("Usuarios.txt");
            Asistente asistente = null;
            for (String usuario:usuarios) {
            String[] listaUsuario = usuario.split(";");
            if (listaUsuario[2].equals(usu)&& listaUsuario[3].equals(con)) {
                asistente=new Asistente(listaUsuario[0], listaUsuario[1], listaUsuario[2], listaUsuario[3],listaUsuario[4].charAt(0));
            }
            }
             System.out.println("\n*********MENU*********\n"
                              + "*******ASISTENTE*******");
             System.out.println("1. Llenar perfil paciente\n"
                     + "2. Cancelar cita\n"
                     + "3. Cancelar usuario\n"
                     + "4. Consultar pacientes con cita");
             System.out.print("Elija una opcion: ");
             int opcion=sc.nextInt();
             while (opcion > 4 || opcion == 0) {
                 System.out.println("Opcion no valida........");
                 System.out.println("\n*********MENU*********\n"
                         + "*******ASISTENTE*******");
                 System.out.println("1. Llenar perfil paciente\n"
                         + "2. Cancelar cita\n"
                         + "3. Cancelar usuario\n"
                         + "4. Consultar pacientes con cita");
                 System.out.print("Elija una opcion: ");
                 opcion = sc.nextInt();   
             }
              if (opcion == 1) {
                  asistente.llenarDatos(usuarios);
             }
             if (opcion == 2) {
                 System.out.println("opcion no programada.....");

             }
             if (opcion == 3) {
                 System.out.println("opcion no programada.......");

             }
             if (opcion == 4) {
                 asistente.consultarCitaPacientes();

             }
             System.out.println("Desea regresa al menu principal (S/N): ");
             String desicion=sc.next().toLowerCase();
             while (desicion.equals("s")) {
             System.out.println("\n*********MENU*********\n"
                              + "*******ASISTENTE*******");
             System.out.println("1. Llenar perfil paciente\n"
                     + "2. Cancelar cita\n"
                     + "3. Cancelar usuario\n"
                     + "4. Consultar pacientes con cita");
             System.out.print("Elija una opcion: ");
             opcion=sc.nextInt();
             while (opcion > 4 || opcion == 0) {
                 System.out.println("opcion no valida......");
                 System.out.println("\n*********MENU*********\n"
                         + "*******ASISTENTE*******");
                 System.out.println("1. Llenar perfil paciente\n"
                         + "2. Cancelar cita\n"
                         + "3. Cancelar usuario\n"
                         + "4. Consultar pacientes con cita");
                 System.out.print("Elija una opcion: ");
                 opcion = sc.nextInt();   
             }
              if (opcion == 1) {
                  asistente.llenarDatos(usuarios);

             }
             if (opcion == 2) {
                 System.out.println("opcion no programada.....");

             }
             if (opcion == 3) {
                 System.out.println("opcion no programada.......");

             }
             if (opcion == 4) {
                 asistente.consultarCitaPacientes();

             }
             System.out.println("Desea regresa al menu principal (S/N): ");
             desicion=sc.next().toLowerCase();
             }
     }
     }
}
